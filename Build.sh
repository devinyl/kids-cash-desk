#!/bin/bash
dotnet publish \
        Units/MediaPlayer.Gui/MediaPlayer.csproj \
        -c Release \
        -o publish \
        -r linux-arm64 \
        -p:PublishSingleFile=true \
        --self-contained true \
        -p:IncludeNativeLibrariesForSelfExtract=true 
        #\        
        #-p:PublishTrimmed=true \
        #-p:PublishReadyToRun=true
