# Child Cash Desk

It's a simple raspberry solution for children to play with.

- It includes a view to play music and audio books
- You can scan barcodes of food products and see what the healthy nutrition score is.

![Embedded in a woodbox](pictures/MusicBoxSmall.jpg)

![Screenshot when scanning a coke can](pictures/NutritionScanScreenshot.png)]

![Screenshot MediaPlayer Overview](pictures/MediaOverview2.png)

![Screenshot Track playing](pictures/MediaPlayerTrack.png)


## What do you need?

- Raspberry Pi 3/4
- DietPi OS configured as described:
    - without any X Server or WindowManager
    - enable VC4-FKMS Video Driver
    - enable ALSA
    - sudo apt-get install libgbm1 libgl1-mesa-dri libegl1-mesa libinput10
    - sudo apt install libfontconfig1
    - sudo apt install ffmpeg
    - sudo apt install avutil
- Barcode Scanner (1D/QR-barcodes) with USB and COM interface (
  e.g. [1680s von ChiY POS Store](https://www.aliexpress.com/item/32902727438.html?spm=a2g0o.9042311.0.0.27424c4d1JqCp9))

## Install the player on the raspberry pi

- clone the code
- cd kids-cash-desk
- ./Build.sh
- cd publish
- scp MediaPlayer dietpi@dietpi:/home/dietpi/
- ssh dietpi@dietpi
- sudo ./MediaPlayer --drm
- *Now you should see the UI showing up*
- Execute sudo dietpi-config
- navigate to:
  9 AutoStart Options -> Custom Script (foreground, with autologin)
- Enter the following in the text file:
  /home/dietpi/MediaPlayer --drm
- Ctrl+x -> Yes
- Select root UID:0 -> OK
- Exit config tool
- reboot with sudo shutdown -h now
- Enjoy!

## Possible Features

Include a page with animal sound. [Animal Sounds](https://freeanimalsounds.org/downloads/)

## Support

Create an issue or mail me.

## Roadmap

Include a page with animal sound. [Animal Sounds](https://freeanimalsounds.org/downloads/)

## License

No specific license except the provided licenses inherited by the integrated open source projects.

## Project status

If there's an interest for a certain feature, just ping me. As it's working and my kids use it, so no need to adapt for the time being. Let's see how it will evolve.

## Notes for issues related to wrong library resolving

Install the following packages (not complete or correct right)

	ffmpeg
	libavcodec-extra
	/libswresample3
	/apt install libavutil56
	libasound2
	libopenal1
	//libavformat-extra
	libicu67

Create symbolic links in the binary folder to the ffmpeg libraries:

	libavcodec.so.58 -> /usr/lib/x86_64-linux-gnu/libavcodec.so.58.54.100
	libavformat.so.58 -> /usr/lib/x86_64-linux-gnu/libavformat.so.58
	libavutil.so.56 -> /usr/lib/x86_64-linux-gnu/libavutil.so.56.31.100
	libswresample.so.3 -> /usr/lib/x86_64-linux-gnu/libswresample.so.3.5.100


	ln -s /usr/lib/x86_64-linux-gnu/libswresample.so.3.5.100 libswresample.so.3
	ln -s  /usr/lib/x86_64-linux-gnu/libavcodec.so.58.54.100 libavcodec.so.58
	ln -s  /usr/lib/x86_64-linux-gnu/libavformat.so.58 libavformat.so.58
	ln -s  /usr/lib/x86_64-linux-gnu/libavutil.so.56.31.100 libavutil.so.56

Raspberry Pi aarch64

	ln -s  /usr/lib/aarch64-linux-gnu/libswresample.so.3.5.100 libswresample.so.3
	ln -s  /usr/lib/aarch64-linux-gnu/libavcodec.so.58.54.100 libavcodec.so.58
	ln -s  /usr/lib/aarch64-linux-gnu/libavformat.so.58 libavformat.so.58
	ln -s  /usr/lib/aarch64-linux-gnu/libavutil.so.56.31.100 libavutil.so.56

ICU missing on dietpi:
https://packages.debian.org/stretch/libicu57, but dotnet is searching for 85-78

Debug *.so loader issues:

      LD_DEBUG=libs DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true  /home/dietpi/MediaPlayer --drm 2> output.txt

      sudo strace -f -t -e trace=file ./SharpAudio.Sample -i "../Music/ChindeMerWaendGoSinge/01 - Schneeglöggli lüüt.mp3"  2> outputStrace.txt

Fix Dllib issue in dotnetcore:

      sudo ln -s /usr/lib/aarch64-linux-gnu/libdl.so.2 /usr/lib/aarch64-linux-gnu/libdl.so

Install usbmount via wget

      http://spui.uk/usbmount/usbmount_0.0.24_all.deb
