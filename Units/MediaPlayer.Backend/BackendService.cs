using System;
using System.Threading;
using System.Threading.Tasks;
using MediaPlayer.Backend.audio;
using MediaPlayer.Backend.Input;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MediaPlayer.Backend
{

    public class BackendService : BackgroundService
    {
        private IServiceScopeFactory _serviceScopeFactory;
        private InputDeviceManager inputMgr;
        private Soundplayer player;
        public BackendService(IServiceScopeFactory serviceScopeFactory, InputDeviceManager inputMgr, Soundplayer player)
        {
            _serviceScopeFactory = serviceScopeFactory;
            this.inputMgr = inputMgr;
            this.player = player;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await this.inputMgr.StartAsync(CancellationToken.None);
            
            this.inputMgr.OnDataReceived += args =>
            {
                Console.WriteLine($"Command received: {args.Text}");
                
                if (args.Text.EndsWith(".mp3"))
                    this.player.LoadAndPlayFile(args.Text);
            };
        }
    }
}