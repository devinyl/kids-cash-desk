using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MediaPlayer.Backend.Input
{

    public class AggregateInputReader : IDisposable, IAggregateInputReader
    {
        private List<InputReader> _readers = new();

        public event InputReader.RaiseKeyPress OnKeyPress;

        private FileSystemWatcher fsw;

        public AggregateInputReader()
        {
            try
            {
                var files = Directory.GetFiles("/dev/input/", "event*");

                fsw = new FileSystemWatcher("/dev/input/", "event*");
                fsw.Created += (obj, eventArgs) =>
                {
                    this.CreateReader(eventArgs.FullPath);
                };
                fsw.Deleted += (obj, eventArgs) =>
                {
                    this.RemoveReader(eventArgs.FullPath);
                };

                foreach (var file in files)
                {
                    this.CreateReader(file);
                }

            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Couldn't read the input directory because of: " + ex.Message);
            }
        }

        private void CreateReader(string file)
        {
            Console.WriteLine("Adding new USB device: : " + file);
            var reader = new InputReader(file);

            reader.OnKeyPress += ReaderOnOnKeyPress;

            _readers.Add(reader);
        }

        private void RemoveReader(string file)
        {
            //if (_readers.SingleOrDefault(rd => rd.))
        }

        private void ReaderOnOnKeyPress(KeyPressEvent e)
        {
            OnKeyPress?.Invoke(e);
        }

        public void Dispose()
        {
            foreach (var d in _readers)
            {
                d.OnKeyPress -= ReaderOnOnKeyPress;
                d.Dispose();
            }

            _readers = null;
        }
    }
}