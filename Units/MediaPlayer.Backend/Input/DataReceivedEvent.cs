using System;

namespace MediaPlayer.Backend.Input
{
    public class DataReceivedEvent : EventArgs
    {
        public DataReceivedEvent(string text)
        {
            this.Text = text;
        }

        public string Text { get; }
    }
}