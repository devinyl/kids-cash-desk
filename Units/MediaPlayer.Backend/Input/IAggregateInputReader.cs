using static MediaPlayer.Backend.Input.InputReader;

namespace MediaPlayer.Backend.Input
{
        public interface IAggregateInputReader
    {
        event RaiseKeyPress OnKeyPress;

        void Dispose();
    }
}