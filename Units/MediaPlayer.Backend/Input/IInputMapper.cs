namespace MediaPlayer.Backend.Input
{
    public interface IInputMapper
    {
        public event EventHandler<SerialDataReceivedEvent> OnSerialDataReceived;

        public string? MapInputString(string serialData);
    }
}