using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MediaPlayer.Backend.Utils;


namespace MediaPlayer.Backend.Input;

public class InputDeviceManager : BackgroundService
{
    public delegate void RaiseDataReceived(DataReceivedEvent args);
    public event RaiseDataReceived OnDataReceived;
    private Settings settings;

    private IAggregateInputReader aggregateReader;
    private SerialReader serialReader;

    private IInputMapper inputMapper;
    private StringBuilder sb = new StringBuilder();

    public InputDeviceManager(Settings settings, SerialReader serialReader, IAggregateInputReader aggregateReader, IInputMapper inputMapper)
    {
        this.settings = settings;
        this.serialReader = serialReader;
        this.aggregateReader = aggregateReader;
        this.inputMapper = inputMapper;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        this.serialReader.OnSerialDataReceived += OnSerialDataReceived;
        this.aggregateReader.OnKeyPress += OnKeyPressed;
        this.inputMapper.OnSerialDataReceived +=  OnSerialMappedDataReceived;
    }

    private void SendSerialData(string text)
    {
        OnDataReceived?.Invoke( new DataReceivedEvent(text));
        string? mappedValue = this.inputMapper.MapInputString(text);
        if(!string.IsNullOrEmpty(mappedValue))
            OnDataReceived?.Invoke( new DataReceivedEvent(mappedValue));
    }

    private void OnSerialDataReceived(SerialDataReceivedEvent data)
    {
        //send new combined event
        if(!string.IsNullOrEmpty(data.Text))
            SendSerialData(data.Text);
            
    }

    public void OnSerialMappedDataReceived(object sender, SerialDataReceivedEvent data)
    {
        OnSerialDataReceived(data);
    }

    private void OnKeyPressed(KeyPressEvent data)
    {
        ConvertCharToString(data);
    }

    private void ConvertCharToString(KeyPressEvent e)
    {
        //System.Console.WriteLine($"Code:{e.Code} State:{e.State}");
        if (e.State == KeyState.KeyUp)
        {
            switch (e.Code)
            {
                case EventCode.Num0:
                    sb.Append(0);
                    break;
                case EventCode.Num1:
                    sb.Append(1);
                    break;
                case EventCode.Num2:
                    sb.Append(2);
                    break;
                case EventCode.Num3:
                    sb.Append(3);
                    break;
                case EventCode.Num4:
                    sb.Append(4);
                    break;
                case EventCode.Num5:
                    sb.Append(5);
                    break;
                case EventCode.Num6:
                    sb.Append(6);
                    break;
                case EventCode.Num7:
                    sb.Append(7);
                    break;
                case EventCode.Num8:
                    sb.Append(8);
                    break;
                case EventCode.Num9:
                    sb.Append(9);
                    break;
                case EventCode.Slash:
                    sb.Append("/");
                    break;
                case EventCode.Backslash:
                    sb.Append("\\");
                    break;
                // case EventCode.Semicolon && EventCode.LeftShift | EventCode.RightShift:
                //     sb.Append("\\");
                //     break;
                // case EventCode.Minus && EventCode.LeftShift | EventCode.RightShift:
                //     sb.Append("_");
                //     break;
                case EventCode.Dot:
                    sb.Append(".");
                    break;
                case EventCode.Minus:
                    sb.Append("-");
                    break;
                case EventCode.Enter:
                    sb.Append(Environment.NewLine);
                    var barcode = sb.ToString();
                    Console.WriteLine("Input received via keyboard: "+ barcode);
                    OnDataReceived?.Invoke( new DataReceivedEvent(barcode));
                    sb.Clear();
                    break;
                default:
                    break;
            }
        } 
    }
}
