using MediaPlayer.Backend.Utils;


namespace MediaPlayer.Backend.Input
{
    public class InputMapper : IInputMapper
    {
        public event EventHandler<SerialDataReceivedEvent> OnSerialDataReceived;
        private Settings settings;

        private readonly Dictionary<string, string> mappings = new Dictionary<string,string>();

        public InputMapper(Settings settings)
        {
            this.settings = settings;

            Console.WriteLine("Checking for inputMappingFile...");
            var fullPath = Path.Combine(this.settings.ProductPath, "Mappings.txt");
            if(File.Exists(fullPath ))
            {
                var lines = File.ReadAllLines(fullPath);
                ParseMappings(lines);
                Console.WriteLine(@"Parsed ${lines.count()} from Mappings.txt");
            }
            else
                Console.WriteLine($"could not find file {fullPath}");
        }

        private void ParseMappings(string[] mappingList)
        {
            foreach (var item in mappingList)
            {
                var keyValue = item.Split("\t");
                if(keyValue.Count() >= 2)
                    this.mappings.Add(keyValue[0], keyValue[1]);
            }
        }

        public string? MapInputString(string serialData)
        {   
            if(this.mappings.ContainsKey(serialData))
                return this.mappings.GetValueOrDefault(serialData);
            else
                return default;

        }

        private void OnDataReceived(DataReceivedEvent dataReceived)
        {
            //send new combined event
            if(!string.IsNullOrEmpty(dataReceived.Text))
            ;
                //OnSerialDataReceived?.Invoke( new SerialDataReceivedEvent(dataReceived.Text));
        }
    }
}