using System;

namespace MediaPlayer.Backend.Input
{
    public class KeyPressEvent : EventArgs
    {
        public KeyPressEvent(EventCode code, KeyState state, char character)
        {
            Code = code;
            State = state;
            Character = character;
        }

        public EventCode Code { get; }

        public KeyState State { get; }

        public char Character { get; }
    }
}