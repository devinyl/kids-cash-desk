        using System.Device.I2c;
        using System.Device.Spi;
        using System.Text;
        using Iot.Device.Card;
        using Iot.Device.Card.Mifare;
        using Iot.Device.Card.Ultralight;
        using Iot.Device.Pn532;
        using Iot.Device.Pn532.ListPassive;
        using Iot.Device.Common;
        using Iot.Device.Ndef;
        using MediaPlayer.Backend.Utils;

        namespace MediaPlayer.Backend.Input
        {
            public class NfcReader
            {
                private Settings settings;
                
                private Pn532 pn532;

                public delegate void RaiseSerialReceived(SerialDataReceivedEvent args);
                public event RaiseSerialReceived OnSerialDataReceived;

                public CancellationToken CancellationToken { get; private set; } = new CancellationToken();

                public NfcReader(Settings settings) => this.settings = settings;

                public async Task Init()
                {
                    try
                    {
                        switch (this.settings.NfcConnectionType)
                        { 
                            case Settings.NfcInterface.Serial:
                                pn532 = new Pn532(this.settings.NFCReaderPn532);
                                break;
                            case Settings.NfcInterface.Spi:
                                int pinSelect;
                                try
                                {
                                    pinSelect = Convert.ToInt32(this.settings.NFCReaderPn532);
                                }
                                catch (Exception ex) when (ex is FormatException || ex is OverflowException)
                                {
                                    Console.WriteLine("Impossible to convert the pin number.");
                                    return;
                                }

                                pn532 = new Pn532(SpiDevice.Create(new SpiConnectionSettings(0) { DataFlow = DataFlow.LsbFirst, Mode = SpiMode.Mode0 }), pinSelect);
                                break;
                            case Settings.NfcInterface.I2c: 
                                pn532 = new Pn532(I2cDevice.Create(new I2cConnectionSettings(1, Pn532.I2cDefaultAddress)));
                                break;
                        }

                        if (pn532.FirmwareVersion is FirmwareVersion version)
                        {
                            Console.WriteLine(
                                $"Is it a PN532!: {version.IsPn532}, Version: {version.Version}, Version supported: {version.VersionSupported}");

                            Task.Run(() => ProcessUltralight(pn532,this.CancellationToken));
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                

                void ProcessUltralight(Pn532 pn532, CancellationToken cancellationToken)
                {
                    byte[]? retData = null;
                    while (!cancellationToken.IsCancellationRequested)
                    {
                        try
                        {
                            retData = pn532.ListPassiveTarget(MaxTarget.One, TargetBaudRate.B106kbpsTypeA);
                            if (retData is object)
                            {
                                continue;
                            }

                            // Give time to PN532 to process
                            Task.Delay(200);


                            if (retData is null)
                            {
                                continue;
                            }

                            for (int i = 0; i < retData.Length; i++)
                            {
                                Console.Write($"{retData[i]:X2} ");
                            }

                            var card = pn532.TryDecode106kbpsTypeA(retData.AsSpan().Slice(1));
                            if (card is not object)
                            {
                                Console.WriteLine("Not a valid card, please try again.");
                                return;
                            }

                            var ultralight = new UltralightCard(pn532!, card.TargetNumber);
                            ultralight.SerialNumber = card.NfcId;
                            Console.WriteLine($"Type: {ultralight.UltralightCardType}, Ndef capacity: {ultralight.NdefCapacity}");

                            var version = ultralight.GetVersion();
                            if ((version != null) && (version.Length > 0))
                            {
                                Console.WriteLine("Get Version details: ");
                                for (int i = 0; i < version.Length; i++)
                                {
                                    Console.Write($"{version[i]:X2} ");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Can't read the version.");
                            }

                            var sign = ultralight.GetSignature();
                            if (sign != null)
                            {
                                Console.WriteLine("Signature: ");
                                for (int i = 0; i < sign.Length; i++)
                                {
                                    Console.Write($"{sign[i]:X2} ");
                                }
                            }

                            // The ReadFast feature can be used as well, note that the PN532 has a limited buffer out of 262 bytes
                            // So maximum 64 pages can be read as once.
                            Console.WriteLine("Fast read example:");
                            var buff = ultralight.ReadFast(0, (byte)(ultralight.NumberBlocks > 64 ? 64 : ultralight.NumberBlocks - 1));
                            if (buff != null)
                            {
                                for (int i = 0; i < buff.Length / 4; i++)
                                {
                                    Console.WriteLine($"  Block {i} - {buff[i * 4]:X2} {buff[i * 4 + 1]:X2} {buff[i * 4 + 2]:X2} {buff[i * 4 + 3]:X2}");
                                }
                            }
                            
                            /*
                            Console.WriteLine("Dump of all the card:");
                            for (int block = 0; block < ultralight.NumberBlocks; block++)
                            {
                                ultralight.BlockNumber = (byte)block; // Safe cast, can't be more than 255
                                ultralight.Command = UltralightCommand.Read16Bytes;
                                var ret = ultralight.RunUltralightCommand();
                                if (ret > 0)
                                {
                                    Console.Write($"  Block: {ultralight.BlockNumber:X2} - ");
                                    for (int i = 0; i < 4; i++)
                                    {
                                        Console.Write($"{ultralight.Data[i]:X2} ");
                                    }

                                    var isReadOnly = ultralight.IsPageReadOnly(ultralight.BlockNumber);
                                    Console.Write($"- Read only: {isReadOnly} ");

                                    Console.WriteLine();
                                }
                                else
                                {
                                    Console.WriteLine("Can't read card");
                                    break;
                                }
                            }

                            Console.WriteLine("Configuration of the card");
                            // Get the Configuration
                            var res = ultralight.TryGetConfiguration(out Configuration configuration);
                            if (res)
                            {
                                Console.WriteLine("  Mirror:");
                                Console.WriteLine($"    {configuration.Mirror.MirrorType}, page: {configuration.Mirror.Page}, position: {configuration.Mirror.Position}");
                                Console.WriteLine("  Authentication:");
                                Console.WriteLine($"    Page req auth: {configuration.Authentication.AuthenticationPageRequirement}, Is auth req for read and write: {configuration.Authentication.IsReadWriteAuthenticationRequired}");
                                Console.WriteLine($"    Is write lock: {configuration.Authentication.IsWritingLocked}, Max num tries: {configuration.Authentication.MaximumNumberOfPossibleTries}");
                                Console.WriteLine("  NFC Counter:");
                                Console.WriteLine($"    Enabled: {configuration.NfcCounter.IsEnabled}, Password protected: {configuration.NfcCounter.IsPasswordProtected}");
                                Console.WriteLine($"  Is strong modulation: {configuration.IsStrongModulation}");
                            }
                            else
                            {
                                Console.WriteLine("Error getting the configuration");
                            }
                            */

                            NdefMessage message;
                            var res = ultralight.TryReadNdefMessage(out message);
                            if (res && message.Length != 0)
                            {
                                foreach (var record in message.Records)
                                {
                                    Console.WriteLine($"Record length: {record.Length}");
                                    if (TextRecord.IsTextRecord(record))
                                    {
                                        var text = new TextRecord(record);
                                        Console.WriteLine(text.Text);
                                        OnSerialDataReceived?.Invoke( new SerialDataReceivedEvent(text.Text));
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("No NDEF message in this card");
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            Task.Delay(1000);
                        }
                    }
                }

                private async Task WriteMessage(string text, UltralightCard ultralight)
                {
                    var res = ultralight.IsFormattedNdef();
                    if (!res)
                    {
                        Console.WriteLine("Card is not NDEF formated, we will try to format it");
                        res = ultralight.FormatNdef();
                        if (!res)
                        {
                            Console.WriteLine("Impossible to format in NDEF, we will still try to write NDEF content.");
                        }
                        else
                        {
                            res = ultralight.IsFormattedNdef();
                            if (res)
                            {
                                Console.WriteLine("Formating successful");
                            }
                            else
                            {
                                Console.WriteLine("Card is not NDEF formated.");
                            }
                        }
                    }

                    NdefMessage newMessage = new NdefMessage();
                    newMessage.Records.Add(new TextRecord("I ❤ .NET IoT", "en", Encoding.UTF8));
                    res = ultralight.WriteNdefMessage(newMessage);
                    if (res)
                    {
                        Console.WriteLine("NDEF data successfully written on the card.");
                    }
                    else
                    {
                        Console.WriteLine("Error writing NDEF data on card");
                    }
                }
            }
        }