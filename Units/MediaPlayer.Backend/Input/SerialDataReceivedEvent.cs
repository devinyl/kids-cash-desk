using System;

namespace MediaPlayer.Backend.Input
{
    public class SerialDataReceivedEvent : EventArgs
    {
        public SerialDataReceivedEvent(string text)
        {
            this.Text = text;
        }

        public string Text { get; }
    }
}