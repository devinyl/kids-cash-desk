using System;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using Avalonia.Input.Raw;
//using Avalonia.Input.Raw;
using MediaPlayer.Backend.Input;
using MediaPlayer.Backend.Utils;

namespace MediaPlayer.Backend.Input
{
    public class SerialReader
    {
        public delegate void RaiseSerialReceived(SerialDataReceivedEvent args);
        public event RaiseSerialReceived OnSerialDataReceived;

        private string selectedPortName;
        public string SelectedPortName
        {
            get { return selectedPortName; }
            set { selectedPortName = value; }
        }

        private SerialPort serialPort;
        private Settings settings;
        public SerialReader(Settings settings)
        {
            this.settings = settings;
            this.SelectedPortName = settings.SerialScannerPort; //"/dev/ttyUSB0";

            //Global Keyboard Events as an alternative solution, insteadof /dev/input/event*
            // var inputMgr = Avalonia.Input.InputManager.Instance;
            // var preProcess = inputMgr.PreProcess;
            // preProcess.Subscribe(OnNext);

            if (this.settings.SerialScannerEnabled)
                this.OpenSerialPortCommand();
        }

        public void OnNext(RawInputEventArgs value)
        {
            Console.WriteLine(value.Device.ToString());
        }

        public void OpenSerialPortCommand()
        {
            try
            {
                var ports = SerialPort.GetPortNames();
                if (!ports.Contains(this.selectedPortName))
                    return;

                //Take the BaseStream of serialport for async operations
                serialPort = new SerialPort(this.SelectedPortName, 115200, Parity.None, 8, StopBits.One);//serialPort = new SerialPort(_selectedPortName);
                serialPort.Handshake = Handshake.None;
                serialPort.ReadTimeout = 500;//Make these two values variables
                serialPort.WriteTimeout = 500;

                //Eventually create a task for datareceived
                serialPort.Encoding = Encoding.UTF8;// System.Text.UTF8Encoding;
                serialPort.DataReceived += serialPort_DataReceived;
                Console.WriteLine("Principal " + Thread.CurrentThread.ManagedThreadId);
                serialPort.Open();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(@$"Couldn't open Serialport '{this.selectedPortName}' with Exception: {ex.Message}");
            }

        }

        //Which thread execute this portion of code?
        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Console.WriteLine("Delegate " + Thread.CurrentThread.ManagedThreadId);
            // Thread.Sleep(10000);
            var line = serialPort.ReadLine();
            if (line.Contains("\r"))
            {
                line = line.Substring(0, line.IndexOf("\r"));
            }
            //Dispatcher.UIThread.Post(() => ReceivedData.Insert(0,line));   
            OnSerialDataReceived?.Invoke(new SerialDataReceivedEvent(line));
        }
    }

}