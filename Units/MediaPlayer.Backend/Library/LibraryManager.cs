using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Avalonia.Threading;
using Avalonia;
using Avalonia.Platform;
using MediaPlayer.Backend.Models;
using Swordfish.NET.Collections;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace MediaPlayer.Backend.Library
{
    public class LibraryManager
    {
        public event EventHandler LibraryScanFinished;
        public ConcurrentObservableCollection<LibraryItem> Items { get; set; } = new ConcurrentObservableCollection<LibraryItem>();

        private Dictionary<string, Avalonia.Media.Imaging.Bitmap> CovertArtCache = new();

        public LibraryItem RootFolder;

        private IAssetLoader assets;

        private string basePath;
        public LibraryManager(string basePath)
        {
            this.assets = AvaloniaLocator.Current.GetService<IAssetLoader>();
            this.basePath = basePath;
            var tmpRoot = new LibraryItem();

            Task.Factory.StartNew(() => { this.ScanFolder(tmpRoot, basePath); this.LibraryScanFinished?.Invoke(this, EventArgs.Empty); }, TaskCreationOptions.LongRunning);

            var start = DateTime.Now;
            while (tmpRoot.Items.Count == 0 && DateTime.Now.Subtract(start) < TimeSpan.FromSeconds(10))
                Task.Delay(100);
            this.RootFolder = tmpRoot.Items.FirstOrDefault();
            this.RootFolder.Parent = null;

        }

        private async void ScanFolder(LibraryItem parent, string directory)
        {
            LibraryItem directoryItem = new LibraryItem();
            directoryItem.IsFolder = true;
            directoryItem.RelativePath = directory.Substring(basePath.Length + (directory.Length > basePath.Length ? 1 : 0));
            directoryItem.FullPath = directory;
            directoryItem.BasePath = basePath;
            directoryItem.Name = new DirectoryInfo(directory).Name;
            directoryItem.Parent = parent;

            parent.Items.Add(directoryItem);

            //Enumerate Directories first
            var dirs = Directory.EnumerateDirectories(directory);
            foreach (var dir in dirs)
            {
                ScanFolder(directoryItem, dir);
            }

            //enumerate all files in dir
            var fileList = new List<LibraryItem>();
            var files = Directory.EnumerateFiles(directory);
            foreach (var file in files)
            {
                var item = new LibraryItem(directoryItem)
                {
                    BasePath = basePath,
                    FullPath = file,
                    Name = Path.GetFileName(file),
                    RelativePath = file.Substring(basePath.Length + (file.Length > basePath.Length ? 1 : 0)),
                    IsFolder = false
                };
                if (file.EndsWith("mp3", true, System.Globalization.CultureInfo.InvariantCulture) ||
                    file.EndsWith("m4a", true, System.Globalization.CultureInfo.InvariantCulture) ||
                    file.EndsWith("flac", true, System.Globalization.CultureInfo.InvariantCulture))
                {
                    this.UpdateTags(item);
                    fileList.Add(item);
                }

            }
            //sort by either the Track Number or otherwise the fileNames
            if (fileList.GroupBy(x => x.TrackNumber).All(g => g.Count() == 1))
            {
                Console.WriteLine($"Sort by TrackNumber of folder {directoryItem.RelativePath}");
                fileList.Sort(delegate (LibraryItem x, LibraryItem y)
                {
                    return x.TrackNumber.CompareTo(y.TrackNumber);
                });
            }
            else
            {
                Console.WriteLine($"Sort by FileName of folder {directoryItem.RelativePath}");
                fileList.Sort();
            }
            fileList.ForEach(x => directoryItem.Items.Add(x));

            if (directoryItem.Items.Count == 0)
                parent.Items.Remove(directoryItem);
            else
            {
                //search for folder picture
                var dirPicture = Path.Combine(directoryItem.FullPath, "folder");
                if (File.Exists(dirPicture + ".jpg"))
                    directoryItem.Picture = new Avalonia.Media.Imaging.Bitmap(dirPicture + ".jpg");
                else if (File.Exists(dirPicture + ".png"))
                    directoryItem.Picture = new Avalonia.Media.Imaging.Bitmap(dirPicture + ".png");
                else if (directoryItem.Items.Any(x => x.Picture != null))
                    directoryItem.Picture = directoryItem.Items.FirstOrDefault(x => x.Picture != null)?.Picture;
            }
        }

        public void UpdateTags(LibraryItem libraryItem)
        {
            try
            {
                Console.WriteLine("Reading Tags of: " + libraryItem.Name);
                var file = TagLib.File.Create(libraryItem.FullPath);

                libraryItem.Title = file.Tag.Title;
                libraryItem.Album = file.Tag.Album;
                libraryItem.Artist = file.Tag.Performers.FirstOrDefault();
                //libraryItem.TrackNumber = (int?)file.Tag.Disc;
                libraryItem.TrackNumber = (int)file.Tag.Track;

                if (file.Tag.Pictures.Length >= 1)
                {
                    try
                    {
                        foreach (TagLib.IPicture picture in file.Tag.Pictures)
                        {
                            // if (picture.Type == TagLib.PictureType.FrontCover)
                            if (picture.MimeType.StartsWith("image/jpeg") || picture.MimeType.StartsWith("image/x-png"))
                            {
                                var hash = GetHashSHA1(picture.Data.Data);
                                if(this.CovertArtCache.ContainsKey(hash))
                                    libraryItem.Picture = this.CovertArtCache[hash];
                                else
                                {
                                    var ms = new MemoryStream(picture.Data.Data);

                                    using var image = Image.Load(ms);
                                    image.Mutate(x => x.Resize(300, 0));
                                    var ns = new MemoryStream();
                                    image.SaveAsBmp(ns);
                                    ns.Position = 0;

                                    //Todo: issue in SkiaSharp https://github.com/AvaloniaUI/Avalonia/issues/7773 
                                    //var bitmap = Avalonia.Media.Imaging.Bitmap.DecodeToWidth(ms, 300);
                                    var bitmap = new Avalonia.Media.Imaging.Bitmap(ns);
                                    this.CovertArtCache.Add(hash, bitmap);
                                    libraryItem.Picture = bitmap;
                                }
                                break;
                            }
                        }
                        // var bin = (byte[])(file.Tag.Pictures[0].Data.Data);
                        // var ms = new MemoryStream(bin);
                        // //var image = Image.FromStream(ms);
                        // var bitmap = new Avalonia.Media.Imaging.Bitmap(ms);
                        // libraryItem.Picture = (Avalonia.Media.Imaging.Bitmap?)bitmap;
                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine($"Exception reading data ID3 stream:  {ex.Message}");
                        libraryItem.Picture = LoadPicture("avares://MediaPlayer/Assets/NoCdCover300.png");
                    }
                }
                else
                {

                    libraryItem.Picture = LoadPicture("avares://MediaPlayer/Assets/NoCdCover300.png");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("could not create bitmap, with exception: {0}", ex.Message);
            }
        }

        private Avalonia.Media.Imaging.Bitmap LoadPicture(string path)
        {
            try
            {
                var bitmap = new Avalonia.Media.Imaging.Bitmap(assets.Open(new Uri(path)));
                return bitmap;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("could not create bitmap, with exception: {0}", ex.Message);
                return null;
            }
        }


        private System.Security.Cryptography.SHA1CryptoServiceProvider hashProvider = new System.Security.Cryptography.SHA1CryptoServiceProvider();

        public string GetHashSHA1(byte[] data)
        {

            return string.Concat(hashProvider.ComputeHash(data).Select(x => x.ToString("X2")));

        }
    }
}