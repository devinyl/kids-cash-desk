using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices; 
using Avalonia.Interactivity;
using Avalonia.Media.Imaging;
using Swordfish.NET.Collections;

namespace MediaPlayer.Backend.Models
{
    public class LibraryItem : IEquatable<LibraryItem>, IComparable<LibraryItem>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<LibraryItem> TappedEvent;
        public bool IsFolder { get; set; }

        public ConcurrentObservableCollection<LibraryItem>? Items { get; set; } = new ConcurrentObservableCollection<LibraryItem>();

        public Bitmap? Picture { get => this.picture; set  { this.picture = value;  NotifyPropertyChanged(); }}

        public string? Title { get => this.title; set  { this.title = value;  NotifyPropertyChanged(); }}
        public string? Album { get; set; }
        public string? Artist { get; set; }
        public int TrackNumber { get; set; }

        public string RelativePath { get; set; }

        public string FullPath { get; set; }

        public string BasePath { get; set; }

        public string Name { get => this.name; set { this.name = value; NotifyPropertyChanged(); } }
        public LibraryItem? Parent { get; set; }

        private Bitmap? picture;

        private string name;

        private string title;

        public LibraryItem() { }

        public LibraryItem(LibraryItem parent)
        {
            this.Parent = parent;
        }

        public void Tapped(object sender, RoutedEventArgs args)
        {
            this.TappedEvent?.Invoke(sender, this);
        }

        public LibraryItem? FindItem(List<string> pathStack)
        {
            var foundItem = this.Items.FirstOrDefault(x => x.Name == pathStack.First());
            if (foundItem != null)
            {
                if (pathStack.Count == 1)
                    return foundItem;
                else
                {
                    pathStack.RemoveAt(0);
                    return foundItem.FindItem(pathStack);
                }
            }
            return null;
        }

        public List<string> GetPathStackPanel(string relativePath)
        {
            var folderStack = relativePath.Split(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            var folderList = Enumerable.ToList(folderStack);
            return folderList;
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")  
        {  
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        } 


        public override string ToString()
        {
            return this.RelativePath;
        }

        public int SortByNameAscending(LibraryItem name1, LibraryItem name2)
        {

            return name1.Name.CompareTo(name2.Name);
        }

        public bool Equals(LibraryItem? other)
        {
            if (other == null) return false;
            return (this.FullPath.Equals(other.FullPath));
        }

        public int CompareTo(LibraryItem? other)
        {
            // A null value means that this object is greater.
            if (other == null)
                return 1;

            else
                return this.FullPath.CompareTo(other.FullPath);
        }
    }
}