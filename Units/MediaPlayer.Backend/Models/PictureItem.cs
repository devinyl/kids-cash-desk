using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices; 
using Avalonia.Interactivity;
using Avalonia.Media.Imaging;
using Swordfish.NET.Collections;

namespace MediaPlayer.Backend.Models
{
    public class PictureItem : IEquatable<PictureItem>, IComparable<PictureItem>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<PictureItem> TappedEvent;
        public bool IsFolder { get; set; }

        public Bitmap? Picture { get => this.picture; set  { this.picture = value;  NotifyPropertyChanged(); }}

        public string? Title { get => this.title; set  { this.title = value;  NotifyPropertyChanged(); }}
        public string RelativePath { get; set; }

        public string FullPath { get; set; }

        public string BasePath { get; set; }

        public PictureItem? Parent { get; set; }

        private Bitmap? picture;

        private string? title;

        public PictureItem() { }

        public PictureItem(PictureItem parent)
        {
            this.Parent = parent;
        }

        public PictureItem(string relativePaht)
        {
            
        }

        public void Tapped(object sender, RoutedEventArgs args)
        {
            this.TappedEvent?.Invoke(sender, this);
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")  
        {  
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        } 


        public override string ToString()
        {
            return this.RelativePath;
        }

        public int SortByNameAscending(PictureItem name1, PictureItem name2)
        {

            return name1.Title.CompareTo(name2.Title);
        }

        public bool Equals(PictureItem? other)
        {
            if (other == null) return false;
            return (this.FullPath.Equals(other.FullPath));
        }

        public int CompareTo(PictureItem? other)
        {
            // A null value means that this object is greater.
            if (other == null)
                return 1;

            else
                return this.FullPath.CompareTo(other.FullPath);
        }
    }
}