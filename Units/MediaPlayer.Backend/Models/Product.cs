using ReactiveUI;

namespace Kasse.Models
{
    public class Product : ReactiveObject
    {
        private string name;
        private string nutritionScore;
        private string ecoscore;
        private string company;

        public string PictureFileName;
        public string Name 
        { 
            get => name; 
            set => this.RaiseAndSetIfChanged(ref name, value); 
        }
        public string NutritionScore 
        { 
            get => nutritionScore; 
            set => this.RaiseAndSetIfChanged(ref nutritionScore, value); 
        }
        public string EcoScore 
        { 
            get => ecoscore; 
            set => this.RaiseAndSetIfChanged(ref ecoscore, value); 
        }

        public string Company 
        { 
            get => company; 
            set => this.RaiseAndSetIfChanged(ref company, value); 
        }
    }
}