﻿using System;
using System.Threading.Tasks;
using MediaPlayer.Backend.audio;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using MediaPlayer.Backend.Input;
using MediaPlayer.Backend.Utils;


namespace MediaPlayer.Backend
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
            .ConfigureServices((hostContext, services) =>
            {
                var settings = new Settings();
                services.AddOptions();
                services.AddSingleton(settings);
                services.AddSingleton(new SerialReader(settings));
                services.AddSingleton<IInputMapper, InputMapper>();
                services.AddSingleton<IAggregateInputReader, AggregateInputReader>();
                services.AddSingleton<InputDeviceManager>();
                services.AddSingleton<Soundplayer>();
                services.AddHostedService<BackendService>();
            });
            await builder.RunConsoleAsync();
        }
    }

}