﻿namespace MediaPlayer.Backend.Utils
{
    /// <summary>
    /// Interface for a configuration loader.
    /// </summary>
    public interface IConfigurationLoader
    {
        /// <summary>
        /// Loads the configuration.
        /// </summary>
        /// <param name="dataPath">The path where the data can be found.</param>
        /// <returns>The loaded application configuration.</returns>
        Settings Load(string dataPath);
    }
}
