using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace MediaPlayer.Backend.Utils
{
    /// <summary>
    /// Loads the application configuration from a JSON data source.
    /// </summary>
    internal sealed class JsonConfigurationLoader  : IConfigurationLoader
    {
        public JsonConfigurationLoader()
        {
        }

        public static readonly string SettingsFileName = "MediaPlayer.Settings.json";
        
        /// <inheritdoc />
        public Settings Load(string dataPath)
        {
            var jsonConfiguration = LoadData(dataPath);

            var serializer = new JsonSerializer {MissingMemberHandling = MissingMemberHandling.Error};
            var configuration = serializer.Deserialize<Settings>(new JTokenReader(jsonConfiguration));

            return configuration;
        }

        private JToken LoadData(string dataPath)
        {
            using var dataReader = new StringReader(File.ReadAllText(dataPath));
            using var jsonReader = new JsonTextReader(dataReader);
            return JToken.Load(jsonReader);
        }

        private void SaveData(string dataPath, Settings settings)
        {
            try
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;
                serializer.Formatting = Formatting.Indented;

                using StreamWriter sw = new StreamWriter(dataPath);
                using JsonWriter writer = new JsonTextWriter(sw);
                serializer.Serialize(writer, settings);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}