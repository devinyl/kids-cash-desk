using System;
using System.Drawing;
using System.IO;
using System.Linq;
using QRCoder;
using PdfSharpCore;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf;
using PdfSharpCore.Pdf.IO;
using System.Diagnostics;
using System.Collections.Generic;
using PdfSharpCore.Drawing.Layout;

namespace MediaPlayer.Backend.Utils
{
    public class QRBarcodeGenerator
    {
        public void GenerateQRCodes(string rootPath, string destPath = null, bool createPDf = true, int labelStartIndex = 1)
        {
            Console.WriteLine($"Generating barcodes from {rootPath}");
            bool relativeStorage = true;
            if (destPath != null)
            {
                relativeStorage = false;
            }
            else
                Console.WriteLine($"Storing QRBarcodes to {destPath}");

            var files = Directory.EnumerateFiles(rootPath, "*.wav", SearchOption.AllDirectories)
            .Union(Directory.EnumerateFiles(rootPath, "*.mp3", SearchOption.AllDirectories))
            .Union(Directory.EnumerateFiles(rootPath, "*.m4a", SearchOption.AllDirectories))
            .Union(Directory.EnumerateFiles(rootPath, "*.flac", SearchOption.AllDirectories));

            if (createPDf)
            {
                var barcodes = this.CreateList(files, rootPath);
                this.GeneratePDF("output.pdf", barcodes, labelStartIndex);
            }
            else
            {
                foreach (var file in files)
                {
                    string relativePath = file.Substring(rootPath.Length + 1);
                    var image = GenerateBarcode(relativePath);

                    var pictureFileName = Path.GetFileNameWithoutExtension(file) + ".png";
                    string imgPath = string.Empty;
                    if (relativeStorage)
                    {
                        imgPath = Path.Combine(Path.GetDirectoryName(file), pictureFileName);
                    }
                    else
                        imgPath = Path.Combine(destPath, pictureFileName);

                    using var fs = File.OpenWrite(imgPath);
                    image.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
                    fs.FlushAsync();
                }
            }
        }

        public IEnumerable<(string, Bitmap)> CreateList(IEnumerable<string> fileNames, string rootPath)
        {
            foreach (var file in fileNames)
            {
                string relativePath = file.Substring(rootPath.Length + 1);
                var image = GenerateBarcode(relativePath);
                yield return (relativePath, image);
            }
        }

        private Bitmap GenerateBarcode(string filePath)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(filePath, QRCodeGenerator.ECCLevel.Q, true);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            return qrCodeImage;
        }

        private void GeneratePDF(string fileName, IEnumerable<(string, Bitmap)> barcodes, int labelStartIndex)
        {
            //Herma 10107 Ablösbare 25mm
            int columns = 6; // 4;
            int rows = 11;//10;

            double borderLeft = XUnit.FromMillimeter(16.1);
            double borderTop = XUnit.FromMillimeter(8.8);

            double boxWidth = XUnit.FromMillimeter(30.48);
            double boxHeight = XUnit.FromMillimeter(25.4);

            double spacingWidth = XUnit.FromMillimeter(0);
            double spacingHeight = XUnit.FromMillimeter(0);

            /*
            AVery Zweckform
            double borderLeft = XUnit.FromMillimeter(8);
            double borderTop = XUnit.FromMillimeter(21.43);
            int columns = 4; // 4;
            int rows = 10;//10;
            double boxWidth = XUnit.FromMillimeter(48.5);
            double boxHeight = XUnit.FromMillimeter(25.4);

            double spacingWidth = XUnit.FromMillimeter(0);
            double spacingHeight = XUnit.FromMillimeter(0);
            */
            // Create a new PDF document
            PdfDocument document = new PdfDocument();
            PageSize[] pageSizes = (PageSize[])Enum.GetValues(typeof(PageSize));


            document.Info.Title = "Created with PDFsharp";


            // Create a font
            XFont font = new XFont("Verdana", 3, XFontStyle.Regular);

            XGraphics gfx = CreateNewPage(document);

            var barcodeList = barcodes.ToList();

            for (int i = 0; i < barcodeList.Count(); i++)
            {
                int indexOnPage = (i + labelStartIndex -1) % (rows * columns);
                if (i != 0 && indexOnPage % (rows * columns) == 0)
                    gfx = CreateNewPage(document);
                DrawImage(gfx, indexOnPage, barcodeList[i].Item2, barcodeList[i].Item1, columns, borderLeft, borderTop, boxWidth, boxHeight, font);
            }

            // Save the document...
            document.Save(fileName);
        }

        public XGraphics CreateNewPage(PdfDocument document)
        {
            // Create an empty page
            PdfPage page = document.AddPage();
            page.Size = PageSize.A4;
            page.Orientation = PageOrientation.Portrait;

            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);
            return gfx;

        }
        void DrawImage(XGraphics gfx, int number, Bitmap bitmap, string caption, int columns, double leftBorder, double topBorder, double boxWidth, double boxHeight, XFont font)
        {
            XImage image = XImage.FromStream( () => 
            { 
                var ms = new MemoryStream();
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png); 
                ms.Position = 0;
                return ms;
            });
            
            // Left position in point
            double x = leftBorder + (number % columns) * boxWidth;
            double y = topBorder + (number / columns) * boxHeight;
            double width = boxWidth > boxHeight? boxHeight : boxWidth;
            gfx.DrawImage(image, x, y, width, width);

            if(width == boxHeight)
            {
                gfx.RotateAtTransform(-90, new XPoint(x + boxWidth, y + boxHeight));
                var tf = new XTextFormatter(gfx);
                //create text on the right
                tf.DrawString(caption, font, XBrushes.Black,
                new XRect(x + boxWidth, y + boxHeight - (boxWidth - width), boxHeight, boxWidth - width),
                XStringFormats.TopLeft);
                gfx.RotateAtTransform(90, new XPoint(x + boxWidth, y + boxHeight));
            }
            else
            {
                Console.WriteLine("Todo: change to show caption under the image");
            }
        }
    }
}