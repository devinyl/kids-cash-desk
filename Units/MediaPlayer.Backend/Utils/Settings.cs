using System;
using System.IO;

namespace MediaPlayer.Backend.Utils
{
    public class Settings
    {
        public string MediaPath { get; set; } = @""; //not allowed to have trailing slash

        public string PicturePath {get; set; } = "";

        public string ProductPath {get;set;} = "products";

        public bool SerialScannerEnabled { get; set; } = true;
        public string SerialScannerPort { get; set; } = "/dev/ttyUSB0";

        public string NFCReaderPn532 {get;set;} = "/dev/ttyAMA0";

        public NfcInterface NfcConnectionType { get; set; } = NfcInterface.Serial; 

        public enum NfcInterface
        {
            Serial,
            Spi,
            I2c
        }

        public Settings()
        {
            if (string.IsNullOrWhiteSpace(this.MediaPath))
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                if (string.IsNullOrWhiteSpace(path))
                    path = Path.Combine(AppContext.BaseDirectory, "Music");
                this.MediaPath = path;
            }
            Console.WriteLine($"using the following media path: {this.MediaPath}");

            if (string.IsNullOrWhiteSpace(this.PicturePath))
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                if (string.IsNullOrWhiteSpace(path))
                    path = Path.Combine(AppContext.BaseDirectory, "Pictures");
                this.PicturePath = path;
            }
            Console.WriteLine($"using the following picture path: {this.PicturePath}");
        }
    }
}