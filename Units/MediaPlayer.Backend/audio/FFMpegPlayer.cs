using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using NetCoreAudio;
using ReactiveUI;
using SharpAudio;
using SharpAudio.Codec;
using SharpGen.Runtime.Win32;

namespace MediaPlayer.Backend.audio
{

    public class FFMpegPlayer : ISoundplayer
    {
        private CompositeDisposable _trackDisposables;
        private CompositeDisposable _internalDisposables;
        private TimeSpan _currentTrackDuration;
        private TimeSpan _currentTrackPosition;
        private bool _IsPlaying;
        private readonly AudioEngine _engine;
        private SoundStream soundstream;

        public delegate void RaiseTrackFinished(EventArgs args);
        public event ISoundplayer.RaiseTrackFinished TrackFinished;
        public TrackStateEnum TrackState { get; set; }
        
        public TimeSpan CurrentTrackDuration
        {
            get => _currentTrackDuration;
            private set => _currentTrackDuration = value;
        }

        public TimeSpan CurrentTrackPosition
        {
            get => _currentTrackPosition;
            private set => _currentTrackPosition = value;
        }

        public FFMpegPlayer()
        {
            Console.WriteLine("Initiating AudioPlayer...");
            PlayTrack();
            _engine = AudioEngine.CreateDefault();

            if (_engine == null) throw new Exception("Failed to create an audio backend!");
            Console.WriteLine("Finished setting up AudioPlayer");
            
            _internalDisposables = new CompositeDisposable();
            
            /*
            this.WhenAnyValue(x => x.TrackState)
                .DistinctUntilChanged()
                .Subscribe(x => IsPlaying = (x == TrackStateEnum.Playing))
                .DisposeWith(_internalDisposables);
            */
        }
        public void LoadAndPlayFile(string path)
        {
            Console.WriteLine($"Playing {path}");

            if (File.Exists(path))
            {
                this.soundstream = new SoundStream(File.OpenRead(path), new SoundSink(_engine));
            }
        }
        
        private async Task PlayTrack()
        {
            _trackDisposables?.Dispose();
            _trackDisposables = new CompositeDisposable();
            _trackDisposables.Add(this.soundstream);

            TrackState = TrackStateEnum.Paused;

            this.soundstream.WhenAnyValue(x => x.State)
                .Subscribe(x =>
                    TrackState = x == SoundStreamState.Playing ? TrackStateEnum.Playing : TrackStateEnum.Paused)
                .DisposeWith(_trackDisposables);

            this.soundstream.WhenAnyValue(x => x.Position)
                .Subscribe(x => this.CurrentTrackPosition = x)
                .DisposeWith(_trackDisposables);

            this.soundstream.WhenAnyValue(x => x.Duration)
                .Subscribe(x => this.CurrentTrackDuration = x)
                .DisposeWith(_trackDisposables);

            /*
            this.soundstream.WhenAnyValue(x => x.State)
                .DistinctUntilChanged()
                .Where(x => x == SoundStreamState.TrackFinished)
                .Take(1)
                .Subscribe(TrackFinished)
                .DisposeWith(_trackDisposables);
            */
            this.soundstream.WhenAnyValue(x => x.State)
                .DistinctUntilChanged()
                .Subscribe(x =>
                    TrackState = x == SoundStreamState.Playing ? TrackStateEnum.Playing : TrackStateEnum.Paused)
                .DisposeWith(_trackDisposables);

            //Todo: update CurrentTrack Property?

            Play();
        }

        public void Play()
        {
            if (TrackState == TrackStateEnum.Paused)
            {
                this.soundstream?.Play();
                TrackState = TrackStateEnum.Playing;
            }
        }

        public void Pause()
        {
            if (TrackState == TrackStateEnum.Paused)
            {
                this.soundstream?.Play();
                TrackState = TrackStateEnum.Playing;
            }
        }

        public void Stop()
        {
            this.soundstream?.Stop();
            TrackState = TrackStateEnum.Paused;
        }

        public void SetVolume(float value)
        {
            if(this.soundstream != null)
            this.soundstream.Volume =value;
        }
        private void OnPlaybackFinished(object sender, EventArgs e)
        {
            Console.WriteLine("Playback finished");
            TrackFinished?.Invoke(this, new EventArgs());
        }

        public void Dispose()
        {
            try
            {
                soundstream?.Dispose();
                _trackDisposables?.Dispose();
                _internalDisposables?.Dispose();
                _engine.Dispose();
            }
            catch (Exception)
            {
            }
        }
    }
}
    