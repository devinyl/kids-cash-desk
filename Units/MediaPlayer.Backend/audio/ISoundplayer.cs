namespace MediaPlayer.Backend.audio;

public enum TrackStateEnum
{
    Playing,
    Paused,
}
public interface ISoundplayer : IDisposable
{
    public event RaiseTrackFinished TrackFinished;
    public TrackStateEnum TrackState { get; set; }
    
    public delegate void RaiseTrackFinished(object sender, EventArgs args);
    void LoadAndPlayFile(string path);
    void Play();
    void Pause();
    void Stop();

    void SetVolume(float volume);
    
    void Dispose();
}