namespace MediaPlayer.Backend.audio;

public class SoundManager : ISoundplayer
{
    private ISoundplayer currentPlayer;
    private ISoundplayer mpg123Player;
    public SoundManager()
    {}

    public event ISoundplayer.RaiseTrackFinished? TrackFinished;
    public TrackStateEnum TrackState { get; set; }

    public void LoadAndPlayFile(string path)
    {
        switch (new FileInfo(path).Extension)
        {
            case ".mp3":
                if(this.currentPlayer is FFMpegPlayer) this.currentPlayer.Dispose();
                if (this.mpg123Player is null)
                {
                    this.currentPlayer = this.mpg123Player = new Soundplayer();
                    this.mpg123Player.TrackFinished += OnPlaybackFinished;
                }
                else
                {
                    this.currentPlayer = this.mpg123Player;
                }
                break;
            default:
                this.currentPlayer = new FFMpegPlayer();                
                break;
        }

        this.currentPlayer.LoadAndPlayFile(path);
    }

  public void Play()
    {
        this.currentPlayer?.Play();
    }

    public void Pause()
    {
        this.currentPlayer?.Pause();
    }

    public void Stop()
    {
        this.currentPlayer?.Stop();
    }

    public void SetVolume(float volume)
    {
        this.currentPlayer?.SetVolume(volume);
    }
    private void OnPlaybackFinished(object sender, EventArgs e)
    {
        Console.WriteLine("Playback finished");
        TrackFinished?.Invoke(this, new EventArgs());
    }

    void ISoundplayer.Dispose()
    {
        this.currentPlayer?.Dispose();
    }

    void IDisposable.Dispose()
    {
        this.currentPlayer?.Dispose();
    }
}
