using System;
using NetCoreAudio;
using SharpGen.Runtime.Win32;

namespace MediaPlayer.Backend.audio
{

    public class Soundplayer : ISoundplayer
    {
        private Player player;


        public event ISoundplayer.RaiseTrackFinished TrackFinished;
        public TrackStateEnum TrackState { get; set; }

        public Soundplayer()
        {
            Console.WriteLine("Initiating AudioPlayer...");
            this.player = new Player();
            this.player.PlaybackFinished += OnPlaybackFinished;
            Console.WriteLine("Finished setting up AudioPlayer");
        }
        public void LoadAndPlayFile(string path)
        {
            Console.WriteLine($"Playing {path}");
            player?.Play(path).Wait();
        }

        public void Play()
        {
            player?.Resume().Wait();
        }

        public void Pause()
        {
            player?.Pause().Wait();
        }

        public void Stop()
        {
            player?.Stop().Wait();
        }

        public void SetVolume(float value)
        {
            player?.SetVolume((byte)(value * 100));
        }
        private void OnPlaybackFinished(object sender, EventArgs e)
        {
            Console.WriteLine("Playback finished");
            TrackFinished?.Invoke(this, new EventArgs());
        }

        public void Dispose()
        {
            player?.Dispose();
        }
    }
}
