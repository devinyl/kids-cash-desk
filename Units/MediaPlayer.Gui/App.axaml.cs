using System;
using System.IO;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using MediaPlayer.Backend.Input;
using Kasse.ViewModels;
using Kasse.Views;
using MediaPlayer.Backend.Utils;
using MediaPlayer.Backend.audio;
using Splat;

namespace Kasse
{
    public class App : Application
    {
        private IAggregateInputReader scannerReader;
        private Settings settings;
        private SerialReader serialReader;
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
        public override void RegisterServices()
        {
            base.RegisterServices();

            this.scannerReader = new AggregateInputReader();
            this.settings = new Settings();
            this.serialReader = new SerialReader(this.settings);

            Splat.Locator.CurrentMutable.RegisterConstant<IAggregateInputReader>(this.scannerReader);
            Splat.Locator.CurrentMutable.RegisterConstant<Settings>(this.settings);
            Splat.Locator.CurrentMutable.RegisterConstant<SerialReader>(this.serialReader);
            Splat.Locator.CurrentMutable.RegisterConstant<ISoundplayer>(new SoundManager());
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if(!Directory.Exists(Path.Combine(AppContext.BaseDirectory, this.settings.ProductPath)))
                Directory.CreateDirectory(Path.Combine(AppContext.BaseDirectory, this.settings.ProductPath));

            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainWindow
                {
                    DataContext = new MainWindowViewModel(this.scannerReader, serialReader),
                };
            }
            else if (ApplicationLifetime is ISingleViewApplicationLifetime singleView)
            {
                singleView.MainView = new MainSingleView
                {
                    DataContext = new MainWindowViewModel(this.scannerReader, serialReader)
                };
            }
            base.OnFrameworkInitializationCompleted();

            System.Console.WriteLine("3st printout");
            System.Console.Write("SkiaSharp.SKTypeface.Default.FamilyName=");
            System.Console.WriteLine(SkiaSharp.SKTypeface.Default.FamilyName);
            System.Console.Write("SkiaSharp.SKFontManager.Default.MatchFamily(null)=");
            System.Console.WriteLine(SkiaSharp.SKFontManager.Default.MatchFamily(null));
            System.Console.Write("SkiaSharp.SKFontManager.Default.MatchFamily(string.Empty)=");
            System.Console.WriteLine(SkiaSharp.SKFontManager.Default.MatchFamily(string.Empty));
            System.Console.Write("SkiaSharp.SKFontManager.Default.MatchFamily(\"sans\")=");
            System.Console.WriteLine(SkiaSharp.SKFontManager.Default.MatchFamily("sans"));
            System.Console.WriteLine("3st printout - done");
        }
    }
}