using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Avalonia.Interactivity;
using Avalonia.Media.Imaging;
using Kasse.ViewModels;
using MediaPlayer.Backend.Models;
using Swordfish.NET.Collections;

namespace MediaPlayer.Gui.Models
{
    public static class LibraryItemExtensions
    {
        public static MediaPlayerViewModel ViewModel;

        public static void Tapped(this LibraryItem libraryItem, object sender, RoutedEventArgs args)
        {
            if(ViewModel != null)
                ViewModel.ButtonPressed(libraryItem.FullPath);
        }
    }
}