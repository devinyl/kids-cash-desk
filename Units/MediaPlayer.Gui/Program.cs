﻿using System;
using System.Linq;
using System.Threading;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.ReactiveUI;
using MediaPlayer.Backend.Utils;

namespace Kasse
{
    class Program
    {
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        [STAThread]
        public static int Main(string[] args)
        {
            if (args.Contains("-qr"))
            {
                var inputFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                var outputFolder = "~/qrcodes";
                if (args.Length == 3)
                {
                    inputFolder = args[1];
                    outputFolder = args[2];
                }
                Console.WriteLine($"Starting creating QR barcodes from {inputFolder} and storing QR barcodes in {outputFolder}");
                QRBarcodeGenerator gen = new QRBarcodeGenerator();
                gen.GenerateQRCodes(inputFolder, outputFolder);
                Console.WriteLine($"Finished creating QR barcodes in folder {outputFolder}");
                return 0;
            }
            else
            {
                if(args.Contains("--i"))
                {
                    
                }
                var builder = BuildAvaloniaApp();
                if (args.Any(arg => arg.Contains("--drm")))
                {
                    SilenceConsole();
                    return builder.StartLinuxDrm(args);
                }
                return builder.StartWithClassicDesktopLifetime(args);
            }
        }

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .UseSkia()
                .LogToTrace(Avalonia.Logging.LogEventLevel.Warning)
                .UseReactiveUI();

        private static void SilenceConsole()
        {
            new Thread(() =>
                {
                    Console.CursorVisible = false;
                    while (true)
                    {
                        Console.WriteLine(Console.ReadLine());
                    }
                })
            { IsBackground = true }.Start();
        }
    }
}
