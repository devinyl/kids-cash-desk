﻿using System;
using System.Collections.Generic;
using Avalonia.Media.Imaging;
using Avalonia.Threading;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Kasse.Models;
using Newtonsoft.Json.Linq;
using ReactiveUI;
using MediaPlayer.Backend.Input;

namespace Kasse.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        static HttpClient client = new HttpClient();

        private static string productDir = "products";

        private readonly string fullProductDir = Path.Combine(AppContext.BaseDirectory, productDir);

        private string ean = string.Empty;
        private Bitmap? picture;
        public string EAN
        {
            get => ean;
            set
            {
                this.RaiseAndSetIfChanged(ref ean, value);
                UpdateEAN(value);
            }
        }

        public string Name
        {
            get => Product?.Name;
        }

        public bool NutriScoreA { get => this.Product?.NutritionScore == "a"; }
        public bool NutriScoreB { get => this.Product?.NutritionScore == "b"; }
        public bool NutriScoreC { get => this.Product?.NutritionScore == "c"; }
        public bool NutriScoreD { get => this.Product?.NutritionScore == "d"; }
        public bool NutriScoreE { get => this.Product?.NutritionScore == "e"; }
        public bool NutriScoreUnknown { get => string.IsNullOrWhiteSpace(this.Product?.NutritionScore); }

        public Bitmap? Picture
        {
            get => picture;
            private set => this.RaiseAndSetIfChanged(ref picture, value);
        }

        private Product product;
        public Product Product
        {
            get => product;
            set => this.RaiseAndSetIfChanged(ref product, value);
        }

        private IAggregateInputReader scanners;
        private SerialReader comReader;
        private StringBuilder sb = new StringBuilder();

        public MainWindowViewModel(IAggregateInputReader inputReader, SerialReader reader)
        {
            this.scanners = inputReader;
            this.comReader = reader;

            scanners.OnKeyPress += (e) =>
            {
                //System.Console.WriteLine($"Code:{e.Code} State:{e.State}");
                if (e.State == KeyState.KeyUp)
                {
                    switch (e.Code)
                    {
                        case EventCode.Num0:
                            sb.Append(0);
                            break;
                        case EventCode.Num1:
                            sb.Append(1);
                            break;
                        case EventCode.Num2:
                            sb.Append(2);
                            break;
                        case EventCode.Num3:
                            sb.Append(3);
                            break;
                        case EventCode.Num4:
                            sb.Append(4);
                            break;
                        case EventCode.Num5:
                            sb.Append(5);
                            break;
                        case EventCode.Num6:
                            sb.Append(6);
                            break;
                        case EventCode.Num7:
                            sb.Append(7);
                            break;
                        case EventCode.Num8:
                            sb.Append(8);
                            break;
                        case EventCode.Num9:
                            sb.Append(9);
                            break;
                        case EventCode.Enter:
                            sb.Append(Environment.NewLine);
                            var barcode = sb.ToString();
                            Console.WriteLine(barcode);
                            Dispatcher.UIThread.InvokeAsync( () => this.UpdateEAN(barcode));
                            sb.Clear();
                            break;
                        default:
                            break;
                    }
                }
            };
            Dispatcher.UIThread.InvokeAsync( () => this.UpdateEAN("5053827153935"));

            this.comReader.OnSerialDataReceived += OnSerialDataReceived;
        }

        public void OnSerialDataReceived(SerialDataReceivedEvent data)
        {
            Console.WriteLine("Serial Data received in ProductVM" + data.Text);
            if(data.Text.Contains("/") || data.Text.Contains(".") )
                return;
            Dispatcher.UIThread.InvokeAsync( () => this.UpdateEAN(data.Text));
        }

        public async void UpdateEAN(string ean)
        {
            string rawJson = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(ean))
                    return;
                //if (!ean.Contains(Environment.NewLine))
                 //   return;
                var eanRaw = ean.Trim();

                string fileName = Path.Combine( fullProductDir,  eanRaw + ".json");
                Console.WriteLine($"Searching for file: {fileName}");

                if (File.Exists(fileName))
                {
                    //load existing file
                    rawJson = await File.ReadAllTextAsync(fileName);
                }
                else
                {
                    var url = String.Format("https://world.openfoodfacts.org/api/v2/product/{0}", eanRaw);
                    //PostAsJsonAsync
                    HttpResponseMessage response = await client.GetAsync(url);
                    //new StringContent(myJson, Encoding.UTF8, "application/json"));
                    response.EnsureSuccessStatusCode();
                    rawJson = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(rawJson);
                    File.WriteAllTextAsync(Path.Combine(fullProductDir, eanRaw + ".json"), rawJson, System.Text.Encoding.UTF8);
                    GetData(rawJson, eanRaw);
                }

                this.Product = MapProduct(rawJson, eanRaw);
                this.LoadPicture(this.Product.PictureFileName);


                this.RaisePropertyChanged("NutriScoreA");
                this.RaisePropertyChanged("NutriScoreB");
                this.RaisePropertyChanged("NutriScoreC");
                this.RaisePropertyChanged("NutriScoreD");
                this.RaisePropertyChanged("NutriScoreE");
                this.RaisePropertyChanged("NutriScoreUnknown");
                this.RaisePropertyChanged("Name");
                // this.ean = string.Empty;
                // this.RaisePropertyChanged("EAN");

            }
            catch (System.Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.WriteLine("RawJson: \n" + rawJson);
            }
            finally
            {
                this.ean = string.Empty;
                this.RaisePropertyChanged("EAN");
            }
        }

        public async Task LoadPicture(string pictureFileName)
        {
            try
            {
                string fullPath = Path.Combine(fullProductDir, pictureFileName);
                if (!string.IsNullOrWhiteSpace(pictureFileName) && File.Exists(fullPath))
                    this.Picture = new Bitmap(fullPath);
                else
                    this.Picture = null;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Product MapProduct(string jsonRaw, string ean)
        {
            JObject o = JObject.Parse(jsonRaw);

            var prod = new Product();
            prod.Name = (string)o.SelectToken("product.product_name");
            prod.NutritionScore = (string)o.SelectToken("product.nutriscore_grade");
            string name = (string)o.SelectToken("product.image_front_url");
            if (!string.IsNullOrWhiteSpace(name))
            {
                prod.PictureFileName = ean + Path.GetExtension(name);
            }
            Console.WriteLine("NutritionScore: " + prod.NutritionScore);
            return prod;
        }

        private async void GetData(string jsonRaw, string eanRaw)
        {
            JObject o = JObject.Parse(jsonRaw);
            string name = (string)o.SelectToken("product.image_front_url");
            if (!string.IsNullOrWhiteSpace(name))
            {
                Bitmap bitmap = await LoadImage(name);
                bitmap.Save(Path.Combine(fullProductDir, eanRaw + Path.GetExtension(name)));
                this.Picture = bitmap;
            }
        }

        public async static Task<Bitmap> LoadImage(string uri)
        {
            Bitmap bitmapImage = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (var response = await client.GetAsync(uri))
                    {
                        response.EnsureSuccessStatusCode();

                        using (Stream inputStream = await response.Content.ReadAsStreamAsync())
                        {
                            bitmapImage = new Bitmap(inputStream);
                        }
                    }
                }
                return bitmapImage;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to load the image: {0}", ex.Message);
            }

            return null;
        }

    }
}
