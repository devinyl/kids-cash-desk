﻿using System;
using System.Collections.Generic;
using Avalonia.Media.Imaging;
using Avalonia.Threading;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Kasse.Models;
using Newtonsoft.Json.Linq;
using ReactiveUI;
using MediaPlayer.Backend.Input;
using SharpAudio;
using SharpAudio.Codec;
using MediaPlayer.Backend.Utils;
using MediaPlayer.Backend.audio;
using System.Drawing;
using System.Linq;
using System.Reactive.Disposables;
using MediaPlayer.Backend.Library;
using System.Collections.ObjectModel;
using MediaPlayer.Backend.Models;
using System.Reactive.Linq;
using Avalonia;
using Swordfish.NET.Collections;
using Avalonia.Interactivity;
using Swordfish.NET.Collections.Auxiliary;

namespace Kasse.ViewModels
{
    public class MediaPlayerViewModel : ViewModelBase, IDisposable
    {
        private static string mediaDir = "music";

        private readonly string fullMediaDir = Path.Combine(AppContext.BaseDirectory, mediaDir);

        private Avalonia.Media.Imaging.Bitmap? picture;
        private string title;
        private string artist;

        private string filePath;

        private bool isListView = true;

        public string FilePath
        {
            get { return this.filePath; }
            set
            {
                this.RaiseAndSetIfChanged(ref filePath, value);
                var fullPath = Path.Combine(this.settings.MediaPath, value);
                if (File.Exists(fullPath))
                {
                    Console.WriteLine("MediaFile found: " + fullPath);
                    this.PlayTrack(fullPath);
                    this.ReadTags(fullPath);
                }
            }
        }

        public string Title
        {
            get => title;
            set
            {
                this.RaiseAndSetIfChanged(ref title, value);
            }
        }

        public string Artist
        {
            get => artist;
            set
            {
                this.RaiseAndSetIfChanged(ref artist, value);
            }
        }
        public Avalonia.Media.Imaging.Bitmap? Picture
        {
            get => picture;
            private set => this.RaiseAndSetIfChanged(ref picture, value);
        }

        private string album;
        public string Album
        {
            get => album;
            set => this.RaiseAndSetIfChanged(ref album, value);
        }

        private bool isPlaying = true;

        public bool IsPlaying
        {
            get => this.isPlaying;
            set => this.RaiseAndSetIfChanged(ref isPlaying, value);
        }
        public float Position
        {
            get { return position; }
            set { this.SetPosition(value); this.RaiseAndSetIfChanged(ref position, value); }
        }
        
        public TimeSpan Duration
        {
            get { return duration; }
            set { this.RaiseAndSetIfChanged(ref duration, value); }
        }

        public float Volume
        {
            get { return volume; }
            set { this.SetVolume(value); this.RaiseAndSetIfChanged(ref volume, value);}
        }
        

        public bool IsListView 
        {
            get => this.isListView;
            set => this.RaiseAndSetIfChanged(ref isListView, value);
        }

        public LibraryItem SelectedTrack { get => this.selectedTrack; set => this.RaiseAndSetIfChanged(ref selectedTrack, value); }
        public ConcurrentObservableCollection<LibraryItem> Items { get; set; }

        public LibraryItem CurrentFolder { get => currentFolder; set => this.RaiseAndSetIfChanged(ref currentFolder, value); }

        public bool IsMediaRoot { get => this.MediaRoot.FullPath == this.CurrentFolder.FullPath; }

        private Vector scrollOffset;
        private DateTime scrollTimeStamp = DateTime.Now;
        public Vector ScrollOffset { get => scrollOffset; set {this.scrollOffset = value; this.scrollTimeStamp = DateTime.Now; Console.WriteLine($"Offset: {value}");} }

        public LibraryItem MediaRoot { get => this.mediaRoot; set => this.RaiseAndSetIfChanged(ref mediaRoot, value); }

        private LibraryItem mediaRoot;
        
        private LibraryItem currentFolder;

        private LibraryItem selectedTrack;

        private IAggregateInputReader scanners;
        private Settings settings;
        private SerialReader serialReader;
        private StringBuilder sb = new StringBuilder();
        private AudioEngine engine;
        private SoundStream soundStream;

        private ISoundplayer soundplayer;

        private CompositeDisposable trackDisposables;

        private float position;
        private float volume;
        private TimeSpan duration;

        public MediaPlayerViewModel(IAggregateInputReader inputReader, SerialReader reader, Settings settings, ISoundplayer soundplayer)
        {
            this.settings = settings;
            this.scanners = inputReader;
            this.serialReader = reader;
            this.soundplayer = soundplayer;
            this.soundplayer.TrackFinished += TrackFinished;

            //LibraryItemExtensions.ViewModel = this;

            LibraryManager libMgr = new LibraryManager(this.settings.MediaPath);

            libMgr.LibraryScanFinished += (sender, args) => 
                {
                    foreach (var item in this.CurrentFolder.Items)
                    {
                        item.TappedEvent += this.Tapped;
                    }
                };

            this.CurrentFolder =  this.MediaRoot = libMgr.RootFolder;

            this.engine = AudioEngine.CreateDefault();

            if (this.engine == null)
            {
                Console.WriteLine("Failed to create an audio backend!");
            }
            Console.WriteLine("Engine initialized with: " + this.engine?.BackendType.ToString());

            //Todo print current selected Sink

            scanners.OnKeyPress += (e) =>
            {
                //System.Console.WriteLine($"Code:{e.Code} State:{e.State}");
                if (e.State == KeyState.KeyUp)
                {
                    switch (e.Code)
                    {
                        case EventCode.Num0:
                            sb.Append(0);
                            break;
                        case EventCode.Num1:
                            sb.Append(1);
                            break;
                        case EventCode.Num2:
                            sb.Append(2);
                            break;
                        case EventCode.Num3:
                            sb.Append(3);
                            break;
                        case EventCode.Num4:
                            sb.Append(4);
                            break;
                        case EventCode.Num5:
                            sb.Append(5);
                            break;
                        case EventCode.Num6:
                            sb.Append(6);
                            break;
                        case EventCode.Num7:
                            sb.Append(7);
                            break;
                        case EventCode.Num8:
                            sb.Append(8);
                            break;
                        case EventCode.Num9:
                            sb.Append(9);
                            break;
                        case EventCode.Enter:
                            sb.Append(Environment.NewLine);
                            var barcode = sb.ToString();
                            Console.WriteLine(barcode);
                            // Dispatcher.UIThread.InvokeAsync(() => this.UpdateEAN(barcode));
                            sb.Clear();
                            break;
                        default:
                            break;
                    }
                }
            };

            this.serialReader.OnSerialDataReceived += OnSerialDataReceived;

            this.Volume = 0.25f;

            //Dispatcher.UIThread.InvokeAsync(() => this.FilePath = "KinderLieder/ChindeMerWaendGoSinge/11 - Roti Rösli im Garte.mp3");
        }

        public void OnSerialDataReceived(SerialDataReceivedEvent data)
        {
            Console.WriteLine("Serial Data received in MediaPlayerVM" + data.Text);
            //this.FilePath = data.Text;
            var pathStack = this.MediaRoot.GetPathStackPanel(data.Text);
            var libItem = this.MediaRoot.FindItem(pathStack);

            this.UpdateList(libItem);
        }

        public void EvaluteCommand(string cmdText)
        {
            //var foundElement = this.Items.FindItem()
        }

        public void Tapped(object sender, LibraryItem libItem)
        {
            if(sender != null)
                this.ButtonPressed( (sender as LibraryItem)?.Name);
        }

        public void PlayCommand()
        {
            if (this.soundStream != null)
            {
                //this.soundStream.Play();
            }
            else
            {
                if(isPlaying)
                {
                    this.soundplayer?.Pause();
                    isPlaying = false;
                }
                else
                {
                    this.soundplayer?.Play();
                    isPlaying = true;
                }
            }
        }

        public void TrackFinished(object sender, EventArgs args)
        {
            this.Next();
        }

        public async void CurrentTrackFinished(SoundStreamState state)
        {
            if (state == SoundStreamState.TrackFinished)
            {
                Console.WriteLine($"Track finished");
                this.Next();
            }
            else
            {
                Console.WriteLine($"State changed: {state.ToString()}");
            }
        }

        public void PositionChanged(TimeSpan position)
        {
            try
            {
                if(position.Ticks != 0)
                {
                    this.position = (float) (position.Ticks / this.Duration.Ticks * 100.0f);
                    Console.WriteLine(position);
                    this.RaisePropertyChanged("Position");
                }
            }
            catch (System.Exception)
            {
            }
        }

        public async void SetPosition(float value)
        {
            if(this.Duration != null)
            {
                var position = new TimeSpan( (long) (value /100f * this.Duration.Ticks));
                //soundStream?.TrySeek(position);
            }
        }

        public void SetVolume(float volume)
        {
            this.soundplayer?.SetVolume(volume);
        }

        public void Next()
        {
            var nextTrack = this.CurrentFolder.Items?.SkipWhile(x => x != this.SelectedTrack).Skip(1).FirstOrDefault(); //DefaultIfEmpty(this.CurrentFolder.Items[0]).
            if (nextTrack != null && !nextTrack.IsFolder)
            {
                Console.WriteLine($"Loading next track: {nextTrack.Title} with FileName {nextTrack.Name} from {nextTrack.RelativePath}");
                this.SelectedTrack = nextTrack;
                this.PlayTrack(this.SelectedTrack.FullPath);
            }
        }

        public void Previous()
        {
            if(this.CurrentFolder?.Items?.Count == 0)
                return;
            var prevTrack = this.CurrentFolder.Items.TakeWhile(x => x != this.SelectedTrack).DefaultIfEmpty( this.CurrentFolder.Items[this.CurrentFolder.Items.Count-1]).LastOrDefault();
            if (prevTrack != null && !prevTrack.IsFolder)
            {
                Console.WriteLine($"Loading previous track: {prevTrack.Title} with FileName {prevTrack.Name} from {prevTrack.RelativePath}");
                this.SelectedTrack = prevTrack;
                this.PlayTrack(this.SelectedTrack.FullPath);
            }
        }

        public async void ButtonPressed(string filePath)
        {
            if(DateTime.Now.Subtract(this.scrollTimeStamp).TotalMilliseconds < 250)
                return;
            Console.WriteLine($"Button pressed: {filePath}");
            var item = this.CurrentFolder.Items.FirstOrDefault(x => x.Name == filePath);
            if (item == null)
                return;

            this.UpdateList(item);
        }

        public void UpdateList(LibraryItem libItem)
        {
            Console.WriteLine("updating list...");
            if(libItem != null)
            {
                RemoveEventHandlers();

                if(libItem.IsFolder)
                {
                    this.CurrentFolder = libItem;
                    if(this.CurrentFolder.Items.All( x => !x.IsFolder))
                    {
                        this.IsListView = false;
                        this.SelectedTrack = libItem.Items.FirstOrDefault();
                    }
                    else
                    {
                        this.IsListView = true;
                        this.SelectedTrack = null;
                    }
                }
                else
                {
                    this.CurrentFolder = libItem.Parent;
                    this.IsListView = false;
                    this.SelectedTrack = libItem;
                }
                Console.WriteLine("Informing about if it's mediaRoot: "+ IsMediaRoot);
                this.RaisePropertyChanged("IsMediaRoot");
                Console.WriteLine("Add EventHandlers...");
                AddEventHandlers();

                Console.WriteLine("Start Playing the following track: "+ SelectedTrack);
                if (this.SelectedTrack != null)
                    this.PlayTrack(this.SelectedTrack.FullPath);
            }
        }

        public void RemoveEventHandlers()
        {
            this.CurrentFolder.Items.ForEach( x => x.TappedEvent -= Tapped);
        }

        public void AddEventHandlers()
        {
            this.CurrentFolder.Items.ForEach( x=> x.TappedEvent += Tapped);
        }

        public async Task PlayTrack(string filePath)
        {
            if(string.IsNullOrWhiteSpace(filePath))
                return;
            Console.WriteLine("Trying to Load Media Track: " + filePath);

            //ffmpeg
            //trackDisposables?.Dispose();
            //this.soundStream = null;
            //trackDisposables = new CompositeDisposable();
            
            //mpg123
            //this.soundplayer?.Stop();
            
            try
            {
                if(filePath.EndsWith(".mp3"))
                {
                    Console.WriteLine("playing with netcoreAudio");
                    PlayNetCoreAudio(filePath);
                }
                else
                {
                    Console.WriteLine("playing with FFMPEG");
                    PlayFFMpeg(filePath);
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine($"Wasn't able to play the track {filePath} with error: {ex.Message} {Environment.NewLine} {ex.StackTrace}");
                this.IsPlaying = false;
            }
        }

        private void PlayNetCoreAudio(string filePath)
        {
            this.soundplayer?.LoadAndPlayFile(filePath);
        }

        private void PlayFFMpeg(string filePath)
        {
            //var soundStream = new SoundStream(File.OpenRead(filePath), engine);
            //trackDisposables.Add(soundStream);
            // this.soundStream.WhenAnyValue(x => x.IsPlaying)
            // .Subscribe(x => this.IsPlaying = x);

            // this.soundStream.WhenAnyValue(x => x.State)
            // .Subscribe(x =>
            //     IsPlaying = x == SoundStreamState.Playing ? true : false);
            // //.DisposeWith(_trackDisposables);

            /*
            soundStream.WhenAnyValue(x => x.State)
                .DistinctUntilChanged()
                .Where(x => x == SoundStreamState.TrackFinished)
                .Take(1)
                .Subscribe(CurrentTrackFinished)
                .DisposeWith(trackDisposables);

            soundStream.WhenAnyValue(x => x.Position)
                .Subscribe(x => this.PositionChanged(x))
                .DisposeWith(trackDisposables);

            soundStream.Play();
            Task.Delay(20);
            this.Duration = soundStream.Duration;
            this.IsPlaying = true;
            this.soundStream = soundStream;
            this.soundStream.Volume = this.Volume;
            */
        }

        public void GoBack()
        {
            Console.WriteLine("Go Back");

            if (this.CurrentFolder.Parent == null)
                return;

            this.UpdateList(this.CurrentFolder.Parent);
        }

        public async Task LoadAlbumPicture(string path)
        {
            try
            {
                string fullPath = Path.Combine(fullMediaDir, path);
                if (!string.IsNullOrWhiteSpace(path) && File.Exists(fullPath))
                    this.Picture = new Avalonia.Media.Imaging.Bitmap(fullPath);
                else
                    this.Picture = null;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ReadTags(string filePath)
        {
            try
            {
                var file = TagLib.File.Create(filePath);

                this.Title = file.Tag.Title;
                this.Album = file.Tag.Album;
                this.Artist = file.Tag.Performers.FirstOrDefault();

                if (file.Tag.Pictures.Length >= 1)
                {
                    var bin = (byte[])(file.Tag.Pictures[0].Data.Data);
                    var ms = new MemoryStream(bin);
                    //var image = Image.FromStream(ms);
                    var bitmap = new Avalonia.Media.Imaging.Bitmap(ms);
                    this.Picture = (Avalonia.Media.Imaging.Bitmap?)bitmap;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("could not create bitmap, with exception: {0}", e.Message);
            }
        }

        public void Dispose()
        {
            this.soundStream?.Dispose();
            this.engine?.Dispose();
            this.soundplayer?.Dispose();
        }
    }
}
