﻿using System;
using System.Collections.Generic;
using Avalonia.Media.Imaging;
using Avalonia.Threading;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Kasse.Models;
using Newtonsoft.Json.Linq;
using ReactiveUI;
using MediaPlayer.Backend.Input;
using SharpAudio;
using SharpAudio.Codec;
using MediaPlayer.Backend.Utils;
using System.Drawing;
using System.Linq;
using System.Reactive.Disposables;
using MediaPlayer.Backend.Library;
using System.Collections.ObjectModel;
using MediaPlayer.Backend.Models;
using System.Reactive.Linq;
using Avalonia;
using Swordfish.NET.Collections;
using Avalonia.Interactivity;
using Swordfish.NET.Collections.Auxiliary;

namespace Kasse.ViewModels
{
    public class PictureViewModel : ViewModelBase, IDisposable
    {
        private Avalonia.Media.Imaging.Bitmap? picture;

        private string filePath;
        private string title;

        private bool isListView = true;

        public string FilePath
        {
            get { return this.filePath; }
            set
            {
                this.RaiseAndSetIfChanged(ref filePath, value);
                var fullPath = Path.Combine(this.settings.MediaPath, value);
                if (File.Exists(fullPath))
                {
                    Console.WriteLine("Pictures found: " + fullPath);
                    //Todo: load Pictures this.ShowPicture(fullPath);
                }
            }
        }

        public string Title
        {
            get => title;
            set
            {
                this.RaiseAndSetIfChanged(ref title, value);
            }
        }

        public Avalonia.Media.Imaging.Bitmap? Picture
        {
            get => picture;
            private set => this.RaiseAndSetIfChanged(ref picture, value);
        }

        private bool isPlaying;

        public bool IsPlaying
        {
            get => this.isPlaying;
            set => this.RaiseAndSetIfChanged(ref isPlaying, value);
        }

        public bool IsListView 
        {
            get => this.isListView;
            set => this.RaiseAndSetIfChanged(ref isListView, value);
        }

        public string SelectedPicture { get => this.selectedPicture; set => this.RaiseAndSetIfChanged(ref selectedPicture, value); }
        public ConcurrentObservableCollection<string> Items { get; set; }

        public DirectoryInfo CurrentFolder { get => currentFolder; set => this.RaiseAndSetIfChanged(ref currentFolder, value); }

        public bool IsPictureRoot { get => this.PictureRoot == this.CurrentFolder.FullName; }

        private Vector scrollOffset;
        private DateTime scrollTimeStamp = DateTime.Now;
        public Vector ScrollOffset { get => scrollOffset; set {this.scrollOffset = value; this.scrollTimeStamp = DateTime.Now; Console.WriteLine($"Offset: {value}");} }

        public string PictureRoot { get => this.pictureRoot; set => this.RaiseAndSetIfChanged(ref pictureRoot, value); }

        private string pictureRoot;
        
        private DirectoryInfo currentFolder;

        private string selectedPicture;

        private IAggregateInputReader scanners;
        private Settings settings;
        private SerialReader serialReader;
        private StringBuilder sb = new StringBuilder();

        public PictureViewModel(IAggregateInputReader inputReader, SerialReader reader, Settings settings)
        {
            this.settings = settings;
            this.scanners = inputReader;
            this.serialReader = reader;

            // libMgr.LibraryScanFinished += (sender, args) => 
            //     {
            //         foreach (var item in this.CurrentFolder.Items)
            //         {
            //             item.TappedEvent += this.Tapped;
            //         }
            //     };

            this.PictureRoot = this.settings.PicturePath;
            this.CurrentFolder = new DirectoryInfo(this.PictureRoot);  

            scanners.OnKeyPress += (e) =>
            {
                //System.Console.WriteLine($"Code:{e.Code} State:{e.State}");
                if (e.State == KeyState.KeyUp)
                {
                    switch (e.Code)
                    {
                        case EventCode.Num0:
                            sb.Append(0);
                            break;
                        case EventCode.Num1:
                            sb.Append(1);
                            break;
                        case EventCode.Num2:
                            sb.Append(2);
                            break;
                        case EventCode.Num3:
                            sb.Append(3);
                            break;
                        case EventCode.Num4:
                            sb.Append(4);
                            break;
                        case EventCode.Num5:
                            sb.Append(5);
                            break;
                        case EventCode.Num6:
                            sb.Append(6);
                            break;
                        case EventCode.Num7:
                            sb.Append(7);
                            break;
                        case EventCode.Num8:
                            sb.Append(8);
                            break;
                        case EventCode.Num9:
                            sb.Append(9);
                            break;
                        case EventCode.Enter:
                            sb.Append(Environment.NewLine);
                            var barcode = sb.ToString();
                            Console.WriteLine(barcode);
                            // Dispatcher.UIThread.InvokeAsync(() => this.UpdateEAN(barcode));
                            sb.Clear();
                            break;
                        default:
                            break;
                    }
                }
            };

            this.serialReader.OnSerialDataReceived += OnSerialDataReceived;
        }

        public void LoadPictures(string directoryPath)
        {
            var dirs = Directory.EnumerateDirectories(directoryPath);
            foreach (var dir in dirs)
            {
                this.Items.Add(dir);
            }

            //enumerate all files in dir
            var fileList = new List<LibraryItem>();
            var files = Directory.EnumerateFiles(directoryPath);
            foreach (var file in files)
            {
            }
        }

        public void OnSerialDataReceived(SerialDataReceivedEvent data)
        {
            Console.WriteLine("Serial Data received in MediaPlayerVM" + data.Text);
            //this.FilePath = data.Text;

            this.UpdateList(data.Text);
        }

        public void EvaluteCommand(string cmdText)
        {
            //var foundElement = this.Items.FindItem()
        }

        public void Tapped(object sender, LibraryItem libItem)
        {
            if(sender != null)
                this.ButtonPressed( (sender as LibraryItem)?.Name);
        }
        public void Next()
        {
            // var nextTrack = this.CurrentFolder.Items?.SkipWhile(x => x != this.SelectedTrack).Skip(1).FirstOrDefault(); //DefaultIfEmpty(this.CurrentFolder.Items[0]).
            // if (nextTrack != null && !nextTrack.IsFolder)
            // {
            //     Console.WriteLine($"Loading next track: {nextTrack.Title} with FileName {nextTrack.Name} from {nextTrack.RelativePath}");
            //     this.SelectedTrack = nextTrack;
            //     Task.Run( () => this.PlayTrack(this.SelectedTrack.FullPath));
            // }
        }

        public void Previous()
        {
            // if(this.CurrentFolder?.Items?.Count == 0)
            //     return;
            // var prevTrack = this.CurrentFolder.Items.TakeWhile(x => x != this.SelectedTrack).DefaultIfEmpty( this.CurrentFolder.Items[this.CurrentFolder.Items.Count-1]).LastOrDefault();
            // if (prevTrack != null && !prevTrack.IsFolder)
            // {
            //     Console.WriteLine($"Loading previous track: {prevTrack.Title} with FileName {prevTrack.Name} from {prevTrack.RelativePath}");
            //     this.SelectedTrack = prevTrack;
            //     Task.Run( () => this.PlayTrack(this.SelectedTrack.FullPath));
            // }
        }

        public async void ButtonPressed(string filePath)
        {
            if(DateTime.Now.Subtract(this.scrollTimeStamp).TotalMilliseconds < 250)
                return;
            Console.WriteLine($"Button pressed: {filePath}");
            
            // var item = this.CurrentFolder.Items.FirstOrDefault(x => x.Name == filePath);
            // if (item == null)
            //     return;

            // this.UpdateList(item);
        }

        public void UpdateList(string path)
        {
            if(path != null)
            {
                // RemoveEventHandlers();

                // if(libItem.IsFolder)
                // {
                //     this.CurrentFolder = libItem;
                //     if(this.CurrentFolder.Items.All( x => !x.IsFolder))
                //     {
                //         this.IsListView = false;
                //         this.SelectedTrack = libItem.Items.FirstOrDefault();
                //     }
                //     else
                //     {
                //         this.IsListView = true;
                //         this.SelectedTrack = null;
                //     }
                // }
                // else
                // {
                //     this.CurrentFolder = libItem.Parent;
                //     this.IsListView = false;
                //     this.SelectedTrack = libItem;
                // }
                // this.RaisePropertyChanged("IsMediaRoot");

                // AddEventHandlers();

                // if(this.SelectedTrack != null)
                //     Task.Run(() => this.PlayTrack(this.SelectedTrack.FullPath));
            }
        }

        public void RemoveEventHandlers()
        {
            //this.CurrentFolder.Items.ForEach( x => x.TappedEvent -= Tapped);
        }

        public void AddEventHandlers()
        {
            // this.CurrentFolder.Items.ForEach( x=> x.TappedEvent += Tapped);
        }

        public async Task PlayTrack(string filePath)
        {
            // if(string.IsNullOrWhiteSpace(filePath))
            //     return;
            // Console.WriteLine("Trying to Load Media Track: " + filePath);

            // trackDisposables?.Dispose();
            // this.soundStream = null;
            // trackDisposables = new CompositeDisposable();
            
            // try
            // {
            //     var soundStream = new SoundStream(File.OpenRead(filePath), engine);
            //     trackDisposables.Add(soundStream);
            //     // this.soundStream.WhenAnyValue(x => x.IsPlaying)
            //     // .Subscribe(x => this.IsPlaying = x);

            //     // this.soundStream.WhenAnyValue(x => x.State)
            //     // .Subscribe(x =>
            //     //     IsPlaying = x == SoundStreamState.Playing ? true : false);
            //     // //.DisposeWith(_trackDisposables);

            //     soundStream.WhenAnyValue(x => x.State)
            //     .DistinctUntilChanged()
            //     .Where(x => x == SoundStreamState.TrackFinished)
            //     .Take(1)
            //     .Subscribe(CurrentTrackFinished)
            //     .DisposeWith(trackDisposables);

            //     soundStream.WhenAnyValue(x => x.Position)
            //     .Subscribe(x => this.PositionChanged(x) )
            //     .DisposeWith(trackDisposables);

            //     soundStream.Play();
            //     this.Duration = soundStream.Duration;
            //     this.IsPlaying = true;
            //     this.soundStream = soundStream;
            // }
            // catch (System.Exception ex)
            // {
            //     Console.WriteLine($"Wasn't able to play the track {filePath} with error: {ex.Message} {Environment.NewLine} {ex.StackTrace}");
            //     this.IsPlaying = false;
            // }
        }

        public void GoBack()
        {
            Console.WriteLine("Go Back");

            // if (this.CurrentFolder.Parent == null)
            //     return;

            // this.UpdateList(this.CurrentFolder.Parent);
        }

        public async Task LoadAlbumPicture(string path)
        {
            try
            {
                string fullPath = Path.Combine(this.CurrentFolder.FullName, path);
                if (!string.IsNullOrWhiteSpace(path) && File.Exists(fullPath))
                    this.Picture = new Avalonia.Media.Imaging.Bitmap(fullPath);
                else
                    this.Picture = null;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Dispose()
        {
        }
    }
}
