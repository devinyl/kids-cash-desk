using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace Kasse.Views
{
    public partial class MainSingleView : UserControl
    {
        public MainSingleView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}