using System;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using Newtonsoft.Json.Linq;

namespace Kasse.Views
{
    public partial class MainView : UserControl
    {
        static HttpClient client = new HttpClient();
        public TextBox txtPassword;
        public MainView()
        {
            InitializeComponent();
#if DEBUG
            //this.AttachDevTools();
#endif

            var textBox = this.FindControl<TextBox>("txtPassword");
            if(textBox != null)
            {
                textBox.AttachedToVisualTree += (s,e) => textBox.Focus();
            }
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        // This requires using Avalonia.Input; 
        private async void txtPassword_KeyPressUp(object sender, KeyEventArgs e)
        {
            if (txtPassword.Text == null)
                return;
            //if(!txtPassword.Text.Contains(Environment.NewLine))
            //return;
            Console.WriteLine(txtPassword.Text);
            if (txtPassword.Text.Length != 13)
                return;
            var eanRaw = txtPassword.Text.Trim();

            var url = String.Format("https://world.openfoodfacts.org/api/v2/product/{0}", eanRaw);
            //PostAsJsonAsync
            HttpResponseMessage response = await client.GetAsync(url);
            //new StringContent(myJson, Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
            var rawJson = await response.Content.ReadAsStringAsync();
            Console.WriteLine(rawJson);
            File.WriteAllTextAsync(eanRaw.Trim() + ".json", rawJson, System.Text.Encoding.UTF8);

            GetData(rawJson, eanRaw);

            // if (txtPassword.Text.Trim().Length > 6)
            // {
            //     btnOK.IsEnabled = true;
            // }
            // else
            // {
            //     btnOK.IsEnabled = false;
            // }
        }

        private async void GetData(string jsonRaw, string eanRaw)
        {
            JObject o = JObject.Parse(jsonRaw);
            string name = (string)o.SelectToken("product.image_front_url");
            Bitmap bitmap = await LoadImage(name);
            bitmap.Save(eanRaw +  "." + Path.GetExtension(name));
        }

        public async static Task<Bitmap> LoadImage(string uri)
        {
            Bitmap bitmapImage = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (var response = await client.GetAsync(uri))
                    {
                        response.EnsureSuccessStatusCode();

                        using (Stream inputStream = await response.Content.ReadAsStreamAsync())
                        {
                            bitmapImage = new Bitmap(inputStream);
                        }
                    }
                }
                return bitmapImage;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to load the image: {0}", ex.Message);
            }

            return null;
        }
    }
}