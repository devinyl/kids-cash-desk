using System;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using Newtonsoft.Json.Linq;

namespace Kasse.Views
{
    public partial class MainWindow : Window
    {
        static HttpClient client = new HttpClient();
        public TextBox txtPassword;
        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif

            var textBox = this.FindControl<TextBox>("txtPassword");
            if(textBox != null)
            {
                textBox.AttachedToVisualTree += (s,e) => textBox.Focus();
            }

            this.KeyUp += (source, args) => { Console.WriteLine("Key pressed " + args.Key);};
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}