using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Kasse.ViewModels;
using MediaPlayer.Backend.Input;
using MediaPlayer.Backend.Utils;
using MediaPlayer.Backend.audio;

namespace Kasse.Views
{
    public partial class MediaPlayerView : UserControl
    {
        public MediaPlayerView()
        {
            InitializeComponent();
            var scanners = (IAggregateInputReader) Splat.Locator.Current.GetService(typeof(IAggregateInputReader));
            var settings = (Settings) Splat.Locator.Current.GetService(typeof(Settings));
            var serialReader = (SerialReader) Splat.Locator.Current.GetService(typeof(SerialReader));
            var soundPlayer = (ISoundplayer) Splat.Locator.Current.GetService(typeof(ISoundplayer));
            this.DataContext = new MediaPlayerViewModel(scanners, serialReader, settings, soundPlayer);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}