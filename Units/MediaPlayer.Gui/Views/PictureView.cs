using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Kasse.ViewModels;
using MediaPlayer.Backend.Input;
using MediaPlayer.Backend.Utils;

namespace Kasse.Views
{
    public partial class PictureView : UserControl
    {
        public PictureView()
        {
            InitializeComponent();
            var scanners = (IAggregateInputReader) Splat.Locator.Current.GetService(typeof(IAggregateInputReader));
            var settings = (Settings) Splat.Locator.Current.GetService(typeof(Settings));
            var serialReader = (SerialReader) Splat.Locator.Current.GetService(typeof(SerialReader));
            this.DataContext = new PictureViewModel(scanners, serialReader, settings);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}